## echarts实现各个国家地图

1.查看demo

serve文件里 执行 nodemon app.js 里面有3个接口 西城区json接口 英国json接口 日本json接口

2.diy制作

world.json里有各个国家的json 需要哪个国家的自己ctrl+f搜英文名找出来 复制‘geometry'对象的内容就行了 该对象里存放着经纬度 创建独立的json文件 如英国 就england.json  红色箭头为变量 根据各个国家来 修改 可以在world.json中找到

![image-20210307133320557](C:\Users\XmX\AppData\Roaming\Typora\typora-user-images\image-20210307133320557.png)

修改index.js的变量 查看不同地区的地图  打开index.html即可 json数据需动态请求 所以我用express起服务器 3000端口 可在浏览器中输入http://localhost:3000/japan 是否请求到json数据 index.js用jquery调接口

添加国家 记得在app.js里添加 一个接口地址 直接复制 改改内容 国家和城市同理 只不过经纬度不一样 json里的 features是个数组对象，一个对象代表一块区域 具体可以查到西城区的案例 11个对象 11个区域块 

![image-20210307133530303](C:\Users\XmX\AppData\Roaming\Typora\typora-user-images\image-20210307133530303.png)

